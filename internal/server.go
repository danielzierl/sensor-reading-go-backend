package internal

import (
	"fmt"
	"os"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
)

const PORT = 3000

func CreateServer() *fiber.App {
	app := fiber.New()
	app.Use(cors.New(cors.Config{
		AllowOrigins: "*",
		AllowHeaders: "Origin, Content-Type, Accept",
	}))
	return app
}
func RunServer(app *fiber.App) {
  var err error
  // should we want to deploy to production we will need a TLS to be able to communicate with TLS secured frontend
  if os.Getenv("DEPLOYMENT_TYPE") == "PROD"{
    err = app.ListenTLS(fmt.Sprintf(":%d", PORT), "/app/fullchain.pem", "/app/privkey.pem")
  }else{
    err=app.Listen(fmt.Sprintf(":%d", PORT))
  }
  if err != nil {
    panic("Could not start server: " + err.Error())
  }
}
