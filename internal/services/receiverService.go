package services

import (
	"log"

	"gitlab.com/danielzierl/sensor-reading-go-backend/internal/resolver"
	"gitlab.com/danielzierl/sensor-reading-shared-libs/models"
)

const ALERT_TEAM_NAME = "red"

func AddSensorReading(sensorReading *models.SensorReading) {
	sensor_reading, err := resolver.AddSensorReading(sensorReading)
	if err != nil {
		log.Printf("Error while adding sensor reading: %v", err)
		log.Println(sensor_reading)
	}
	if sensor_reading.TeamName == ALERT_TEAM_NAME {
    InspectSensorReadingAlert(sensor_reading)
	}
}
