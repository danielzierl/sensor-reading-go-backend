#!/bin/bash

# Define the directories and repository URLs for the three projects
receiver_dir="/home/gitlab-runner/mqtt-go-service-receiver"
receiver_repo="https://gitlab.com/danielzierl/mqtt-go-service-receiver.git"

backend_dir="/home/gitlab-runner/sensor-reading-go-backend"
backend_repo="https://gitlab.com/danielzierl/sensor-reading-go-backend.git"

frontend_dir="/home/gitlab-runner/sensor-reading-svelte-frontend"
frontend_repo="https://gitlab.com/danielzierl/sensor-reading-svelte-frontend.git"

# Function to pull from Git repositories
git_pull() {
  local dir="$1"
  local repo="$2"
  echo "Pulling from $repo into $dir..."
  if [ -d "$dir" ]; then
    cd "$dir" || exit
    git pull
  else
    git clone "$repo" "$dir"
  fi
}

# Function to run Docker Compose
run_docker_compose() {
  echo "Running Docker Compose in the current directory..."
  docker-compose -f docker-compose.yaml up 
}

# Pull from Git repositories for each project
git_pull "$receiver_dir" "$receiver_repo"
git_pull "$backend_dir" "$backend_repo"
git_pull "$frontend_dir" "$frontend_repo"

# Run Docker Compose in the current directory
run_docker_compose