package services

import (
	"log"

	"gitlab.com/danielzierl/sensor-reading-go-backend/internal/resolver"
	"gitlab.com/danielzierl/sensor-reading-shared-libs/models"
)

func GetAllLogs() *[]models.Log{
	logs, err := resolver.GetLogs()
  if err != nil {
    log.Printf("Error getting all logs: %v", err)
    return nil
  }
	return logs 
}
