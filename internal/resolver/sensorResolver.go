package resolver

import (
	"fmt"

	"gitlab.com/danielzierl/sensor-reading-shared-libs/models"
)

func RefreshSensors(sensors []models.Sensor) {
	fmt.Println("Refreshing sensors")
	for _, sensor := range sensors {
		db.Save(&sensor)
	}
	SetupSensors()
}

// Reguired in alertsResolver
const Temperature_sensor_name = "sensor11_temperature"
const Humidity_sensor_name = "sensor12_humidity"
const Illumination_sensor_name = "sensor13_illumination"

var Temperature_sensor models.Sensor
var Humidity_sensor models.Sensor
var Illumination_sensor models.Sensor

func SetupSensors() {
	db.Where("name = ?", Temperature_sensor_name).First(&Temperature_sensor)
	db.Where("name = ?", Humidity_sensor_name).First(&Humidity_sensor)
	db.Where("name = ?", Illumination_sensor_name).First(&Illumination_sensor)
}
