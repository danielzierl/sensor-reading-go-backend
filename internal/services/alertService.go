package services

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"

	"gitlab.com/danielzierl/sensor-reading-go-backend/internal/resolver"
	"gitlab.com/danielzierl/sensor-reading-shared-libs/models"
)
const TEMP_DIF_INSENSITIVITY float64 = 0.5
const HUM_DIF_INSENSITIVITY float64 = 3
const ILLUM_DIF_INSENSITIVITY float64 = 0.5
// ugly ahhh code, better not look, i could just join the variables, but it's too much work
var last_temperature_was_alert_pirate_flag bool = false
var last_temp_id_alert uint = 0
var last_humidity_was_alert_pirate_flag bool = false
var last_hum_id_alert uint = 0
var last_illumination_was_alert_pirate_flag bool = false
var last_illumination_id_alert uint = 0

func InspectSensorReadingAlert(sensorReading *models.SensorReading) {
	if sensorReading.Illumination != 0 {
		meassured_value := sensorReading.Illumination
		sensor := resolver.Illumination_sensor
		checkIfSensorReadingInAlert(meassured_value, &sensor, &last_illumination_was_alert_pirate_flag, &last_illumination_id_alert, "Illumination", ILLUM_DIF_INSENSITIVITY)
	}
	if sensorReading.Humidity != 0 {
		meassured_value := sensorReading.Humidity
		sensor := resolver.Humidity_sensor
		checkIfSensorReadingInAlert(meassured_value, &sensor, &last_humidity_was_alert_pirate_flag, &last_hum_id_alert, "Humidity", HUM_DIF_INSENSITIVITY)
	}
	if sensorReading.Temperature != 0 {
		meassured_value := sensorReading.Temperature
		sensor := resolver.Temperature_sensor
		checkIfSensorReadingInAlert(meassured_value, &sensor, &last_temperature_was_alert_pirate_flag, &last_temp_id_alert, "Temperature", TEMP_DIF_INSENSITIVITY)
	}

}

func checkIfSensorReadingInAlert(meassured_value float64, sensor *models.Sensor, last_alert_pirate_flag *bool, last_alert_id *uint, Type string, insensitivity float64) {
	if meassured_value < sensor.MinTemperature || meassured_value > sensor.MaxTemperature {
		var event string
		if meassured_value < sensor.MinTemperature {
			event = "low"
		} else {
			event = "high"
		}
		if *last_alert_pirate_flag {
			return
		} else {
			*last_alert_pirate_flag = true
		}

		alertID, err := resolver.AddAlert(meassured_value, sensor, Type, event)
    sendGotifyAlert(Type, event)
    *last_alert_id = *alertID
		if err != nil {
			log.Printf("Error while adding alert to DB: %v", err)
		}
	} else if (*last_alert_pirate_flag && (meassured_value > sensor.MinTemperature+insensitivity || meassured_value < sensor.MaxTemperature-insensitivity)) {
    resolver.EndAlert(*last_alert_id)
    sendGotifyAlert(Type, "alert ended")
		*last_alert_pirate_flag = false
	}
}
func GetAlerts() ([]models.Alert, error) {
  return resolver.GetAlerts()
}
type AlertGotify struct {
  Title string `json:"title"`
  Message string `json:"message"`
  Priority int `json:"priority"`
}
func sendGotifyAlert(Type string, event string) {
  fmt.Println("sending alert")
  formData:= AlertGotify{
    Title: fmt.Sprintf("%s alert", Type),
    Message: fmt.Sprintf("%s %s", Type, event),
    Priority: 0,
  }
  reqBody, err := json.Marshal(formData)
  if err != nil {
    log.Printf("Could not marshal json: %v", err)
  }
  url := fmt.Sprintf("http://sulis78.zcu.cz/message?token=%s", os.Getenv("GOTIFY_TOKEN"))
  resp, err := http.Post(url, "application/json", bytes.NewBuffer(reqBody))
  if err != nil {
    log.Printf("Could not send alert: %v", err)
    return
  }
  defer resp.Body.Close()
}
