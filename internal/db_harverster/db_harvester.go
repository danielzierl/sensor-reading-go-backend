package dbharverster

import (
	"fmt"
	"os"
	"strconv"
	"time"

	"gitlab.com/danielzierl/sensor-reading-go-backend/internal/resolver"
	"gitlab.com/danielzierl/sensor-reading-shared-libs/models"
	"gorm.io/gorm"
)

func HarvestDb(){
  db:=resolver.GetDB()
  modulo,_ :=  strconv.Atoi(os.Getenv("HARVESTING_MODULO"))
  time_back := -3600 * time.Second
  for {
    fmt.Println("Harvesting db")

    db.Where("team_name = 'red' AND created_at < ? AND consecutive_prune % ? <> 0", time.Now().Add(time_back), modulo).Delete(&models.SensorReading{})
    db.Model(&models.SensorReading{}).Where("team_name = 'red' AND created_at < ? AND consecutive_prune % ? = 0 AND consecutive = consecutive_prune", time.Now().Add(time_back), modulo).Updates(map[string]interface{}{
      "consecutive": gorm.Expr("consecutive / ?", modulo),
    })


    time.Sleep(3600 * time.Second)
  }
}
