package resolver

import (
	"log"
	"time"

	"gitlab.com/danielzierl/sensor-reading-shared-libs/models"
)

func AddAlert(meassured_value float64, sensor *models.Sensor, Type string, event string) (*uint, error) {
	alert := models.Alert{
		MeasuredValue: meassured_value,
		Sensor:        *sensor,
		Type:          Type,
		Event:         event,
	}

	result := db.Create(&alert)
	if result.Error != nil {
		return nil, result.Error
	}
	return &alert.ID, nil
}
func EndAllAlerts() {
	var alerts []models.Alert
	db.Where("\"end\" IS NULL").Find(&alerts)
	for _, alert := range alerts {
		EndAlert(alert.ID)
	}
}
func EndAlert(alertId uint) error {

	var existingAlert models.Alert
	res := db.First(&existingAlert, alertId)
	if res.Error != nil {
		log.Printf("Error while ending alert: %v", res.Error)
		return res.Error
	}
	existingAlert.End = time.Now()
	result := db.Save(&existingAlert)
	if result.Error != nil {
		log.Printf("Error while ending alert: %v", res.Error)
		return result.Error
	}
	return nil
}
func GetAlerts() ([]models.Alert, error) {
	var alerts_temp []models.Alert
	var alerts_humidity []models.Alert
	result := db.Limit(5).Where("type = ?", "Temperature").Order("created_at DESC").Find(&alerts_temp)
	if result.Error != nil {
		return nil, result.Error
	}
	result = db.Limit(5).Where("type = ?", "Humidity").Order("created_at DESC").Find(&alerts_humidity)
	if result.Error != nil {
		return nil, result.Error
	}
	return append(alerts_temp, alerts_humidity...), nil
}
