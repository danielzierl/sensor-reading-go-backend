package controllers

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"

	"github.com/dgrijalva/jwt-go"
	"github.com/gofiber/fiber/v2"
	"gitlab.com/danielzierl/sensor-reading-go-backend/internal/services"
)
type LoginResponse struct {
  Token string `json:"token,omitempty"`
}

func SetLoginRoutes(app *fiber.App) {
	app.Get("/verify", (VerifyTokenEndpoint))
	app.Post("/login-base", (LoginBase))
	app.Post("/login-face", (LoginFace))
}
func LoginBase(c *fiber.Ctx) error {
	body := c.Body()
	resp_body,err := forwardLoginBase(body)
	if err != nil {
		log.Println(err)
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"error": err.Error(),
		})
	}
  fmt.Println(resp_body.Token)
	return c.Status(fiber.StatusOK).JSON(fiber.Map{
    "token": resp_body.Token,
	})

}
func LoginFace(c *fiber.Ctx) error{
	url := os.Getenv("LOGIN_SERVICE_URL")
  body := c.Body()
  resp,err:=http.Post(fmt.Sprintf("%s/recognize", url), "application/json", bytes.NewReader(body))
  if err != nil {
    log.Println("login face error: ",err)
    return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
      "error": err.Error(),
    })
  }
  resp_body, err := io.ReadAll(resp.Body)
  if err != nil {
    log.Println("login face error: ",err)
    return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
      "error": err.Error(),
    })
  }

  if resp.StatusCode != 200 {
    log.Println("login face error: ",resp.StatusCode, err)
    return c.Status(fiber.StatusInternalServerError).JSON(resp_body)
  }
  return c.Status(fiber.StatusOK).SendString(string(resp_body))

}
func forwardLoginBase(body []byte) (*LoginResponse, error) {
	url := os.Getenv("LOGIN_SERVICE_URL")
	resp, err := http.Post(fmt.Sprintf("%s/default_login", url), "application/json", bytes.NewReader(body))
  if err != nil {
    log.Println("login default: ",err)
    return nil, err
  }
  if resp.StatusCode != 200 {
    log.Println("login default: ",resp.StatusCode)
    return nil, errors.New("login base post returned non 200 status code, login was not successfull")
  }
  var resp_body_struct LoginResponse
  defer resp.Body.Close()
  resp_body, err := io.ReadAll(resp.Body)
  if err != nil {
    log.Println("login default: ",err)
    return nil, err
  }

  err=json.Unmarshal(resp_body, &resp_body_struct)
  if err != nil {
    log.Println("login default unmarshal: ",err)
  }
	return &resp_body_struct,err

}

func VerifyTokenEndpoint(c *fiber.Ctx) error {
  jwt_secret, err := services.GetTokenSecret()
  if err != nil {
    return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
      "error": err.Error(),
    })
  }
  tokenString := c.Query("token")
  if tokenString == "" {
    return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
      "error": "Token is missing",
    })
  }

  // Parse and verify the JWT token
  err = VerifyToken(jwt_secret, tokenString)

  if err != nil {
    return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{
      "error": "Token is invalid",
      "authenticated": false,
    })
  }

  fmt.Println("Token is valid")
  return c.Status(fiber.StatusOK).JSON(fiber.Map{
    "message": "Token is valid",
    "authenticated": true,
  })

}
func VerifyToken(jwt_secret []byte, tokenString string) error {
  _, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
    return jwt_secret, nil
  })
  return err

}
