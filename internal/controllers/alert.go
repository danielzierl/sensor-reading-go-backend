package controllers

import (
	"encoding/json"
	"log"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/websocket/v2"
	"gitlab.com/danielzierl/sensor-reading-go-backend/internal/services"
)

func SetAlertRoutes(app *fiber.App) {
	app.Get("/alerts", websocket.New(GetAlerts))
}


func GetAlerts(c *websocket.Conn) {
  defer c.Close()
  for {
    alerts,err := services.GetAlerts()
    if err != nil {
      log.Printf("Could not get alerts: %v", err)
      continue
    }
    jsonData, err := json.Marshal(alerts)
    if err != nil {
      log.Printf("Could not marshal json: %v", err)
      continue
    }
    err = c.WriteMessage(websocket.TextMessage, jsonData)
		if err != nil {
      // client disconnected, lets not log it, it makes mess in the logs, pipes get broken
      break
		}
    if _,_,err = c.ReadMessage(); err !=nil {
      break
    }



  }
}
