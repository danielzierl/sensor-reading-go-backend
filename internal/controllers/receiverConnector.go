package controllers

import (
	"log"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/danielzierl/sensor-reading-go-backend/internal/services"
	"gitlab.com/danielzierl/sensor-reading-shared-libs/models"
)

func SetRecieverConnectorRoutes(app *fiber.App) {
	app.Post("/readings", receiveSensorReadingFromReciever)
}

func receiveSensorReadingFromReciever(c *fiber.Ctx) error {
	var recievedSensorReading models.SensorReading
	err := c.BodyParser(&recievedSensorReading)
	if err != nil {
		log.Printf("Could not parse body from temp post: %v", err)
		return c.SendStatus(400)
	}

	services.AddSensorReading(&recievedSensorReading)
	return c.SendStatus(200)
}
