package caching

import (
	"fmt"
	"time"

	"github.com/patrickmn/go-cache"
	specialtypes "gitlab.com/danielzierl/sensor-reading-go-backend/internal/special_types"
	"gitlab.com/danielzierl/sensor-reading-shared-libs/models"
	"gorm.io/gorm"
)

var cacher *cache.Cache
func SetupCacher() error {
  cacher = cache.New( time.Millisecond*1000,  time.Millisecond*5000)
  return nil
}
func FindCachedReading(key string, db *gorm.DB, loc *[]specialtypes.FrontendSensorReading) {
  item, found := cacher.Get(key)
  if found {
    fmt.Println("Found in cache")
    if cached, ok :=item.([]specialtypes.FrontendSensorReading); ok {
      *loc = cached
    }
    return
  }
  db.Find(loc)
  cacher.Set(key, *loc, cache.DefaultExpiration)
}
func CacheTeamLastReading(key string, db *gorm.DB, last_reading *models.SensorReading, team string) {
  item, found := cacher.Get(key)
  if found {
    fmt.Println("Found in cache last reading")
    if cached, ok :=item.(models.SensorReading); ok {
      *last_reading= cached
    }
    return
  }
  db.Limit(500).Where("team_name = ?", team).Order("created_at DESC").First(&last_reading)
  cacher.Set(key, *last_reading, time.Millisecond*200)
}



