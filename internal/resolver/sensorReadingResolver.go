package resolver

import (
	"log"
	"os"

	// "os"
	"strconv"
	"time"

	"gitlab.com/danielzierl/sensor-reading-shared-libs/models"
	"gorm.io/gorm"
)

func GetAllTemperatureReadings(table string) (*gorm.DB, error) {
	result := db.Table(table).Select("temperature as value, timestamp as created_at").Order("created_at DESC").Where("temperature IS NOT NULL AND deleted_at IS NULL")
	if result.Error != nil {
		return nil, result.Error
	}
	return result, nil
}

func GetAllHumidityReadings(table string) (*gorm.DB, error) {
	result := db.Table(table).Select("humidity as value, timestamp as created_at").Order("created_at DESC").Where("humidity IS NOT NULL AND deleted_at IS NULL")
	if result.Error != nil {
		return nil, result.Error
	}
	return result, nil
}

func TeamFinder(team string, db *gorm.DB) *gorm.DB {
	return db.Where("team_name = ?", team)

}

func ResultLimiter(n_results string, db *gorm.DB) *gorm.DB {
	integer_n, err := strconv.Atoi(n_results)
	if err != nil {
		log.Printf("Error converting n_results to integer: %v", err)
	}
	return db.Limit(integer_n)

}
func LinearTimeframe(time_frame_hours string, n_results string, db *gorm.DB) *gorm.DB {
	integer_time_frame_hours, err := strconv.ParseFloat(time_frame_hours, 64)
	if err != nil {
		log.Printf("Error converting time_frame_hours to integer: %v", err)
	}
	integer_n, err := strconv.ParseInt(n_results, 10, 64)
	if err != nil {
		log.Printf("Error converting n_results to integer: %v", err)
	}
	// Calculate the time threshold for the last N hours
	nHoursAgo := time.Now().Add(-time.Duration(integer_time_frame_hours * 60 * 60) * time.Second)
  db=db.Where("created_at >= ?", nHoursAgo)


	// now i wonder how many results have i got
	var countNoHarvest int64
	var countHarvested int64
  sessionNoHarvest := db.Session(&gorm.Session{FullSaveAssociations: true})
  sessionHarvested := db.Session(&gorm.Session{FullSaveAssociations: true})
	sessionNoHarvest.Model(&models.SensorReading{}).Where("consecutive = consecutive_prune").Count(&countNoHarvest)
  sessionHarvested.Model(&models.SensorReading{}).Where("consecutive <> consecutive_prune").Count(&countHarvested)


	// since i know how many results i have and how many i want, lets find a nice module that can skip the necessary amount of results

  harvesting_modulo, _ := strconv.ParseInt(os.Getenv("HARVESTING_MODULO"), 10, 64)
  // var harvesting_modulo int64 = 10
	modulo := (countNoHarvest+countHarvested*harvesting_modulo)/integer_n + 1 // division by zero is fun, but lets not
  db=db.Where("( consecutive % ? = 0 AND consecutive = consecutive_prune ) OR ( consecutive % ? = 0 AND consecutive <> consecutive_prune )",modulo, int(modulo/harvesting_modulo)+1)

	return db

}
