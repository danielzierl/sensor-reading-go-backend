FROM golang:1.21.1-alpine
WORKDIR /app
ENV CGO_ENABLED=1
RUN apk add gcc libc-dev
COPY . .
RUN go build -o backend ./cmd/main/main.go
CMD ["./backend"]
