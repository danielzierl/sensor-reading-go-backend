package resolver

import "gitlab.com/danielzierl/sensor-reading-shared-libs/models"
func GetLogs() (*[]models.Log, error){

	var logs []models.Log
	result := db_log.Order("created_at DESC").Limit(1000).Find(&logs)
	if result.Error != nil {
		return nil, result.Error
	}
	return &logs, nil
}
