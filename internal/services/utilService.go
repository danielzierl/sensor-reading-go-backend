package services

import (
	"fmt"
	"log"

	linuxproc "github.com/c9s/goprocinfo/linux"
	"github.com/shirou/gopsutil/v3/mem"
	"gitlab.com/danielzierl/sensor-reading-go-backend/internal/resolver"
)
var idle uint64=0
var user uint64=0
func GetCpuUsage() string {
  stat, err := linuxproc.ReadStat("/proc/stat")
  if err != nil {
    log.Fatal("stat read fail")
  }
  var deltaUser uint64 = stat.CPUStatAll.User - user
  var deltaIdle uint64 = stat.CPUStatAll.Idle - idle
  user = stat.CPUStatAll.User
  idle = stat.CPUStatAll.Idle
  return fmt.Sprintf("%.2f",100*float64(deltaUser)/float64(deltaIdle+deltaUser))
}


func GetRamUsage() string{
  v,_:=mem.VirtualMemory()
  return fmt.Sprintf("%.2f",float64(v.UsedPercent))
}
func GetReadingsCount() string{
  return fmt.Sprintf("%d",resolver.GetReadingsCount())
}
