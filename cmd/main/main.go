package main

import (
	"log"
	"os"

	"github.com/joho/godotenv"
	"gitlab.com/danielzierl/sensor-reading-go-backend/internal"
	"gitlab.com/danielzierl/sensor-reading-go-backend/internal/caching"
	"gitlab.com/danielzierl/sensor-reading-go-backend/internal/controllers"
	dbharverster "gitlab.com/danielzierl/sensor-reading-go-backend/internal/db_harverster"
	"gitlab.com/danielzierl/sensor-reading-go-backend/internal/resolver"
	"gitlab.com/danielzierl/sensor-reading-go-backend/internal/services"
	"gitlab.com/danielzierl/sensor-reading-shared-libs/logging"
)

func main() {
	logging.SetupLoggingAuto("backend")
	defer logging.CloseLogFile()

	err := godotenv.Load()
	if err != nil {
		log.Panic(".env file not found")
	}
	app := internal.CreateServer()
	resolver.SetupAndConnectDb()
	if os.Getenv("DISABLE_AWS_LOGIN") == "1" {

	} else {
		services.ReadAllSensors()
	}
  caching.SetupCacher()
  go dbharverster.HarvestDb()
	controllers.SetSensorReadingRoutes(app)
	controllers.SetUtilRoutes(app)
	controllers.SetLoginRoutes(app)
	controllers.SetLogRoutes(app)
	controllers.SetRecieverConnectorRoutes(app)
	controllers.SetAlertRoutes(app)
	resolver.EndAllAlerts()
	internal.RunServer(app)
}
