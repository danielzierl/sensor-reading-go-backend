package resolver

import (
	"gitlab.com/danielzierl/sensor-reading-shared-libs/models"
)

func AddSensorReading(reading *models.SensorReading) (*models.SensorReading, error) {
	result := db.Create(reading)
	if result.Error != nil {
		return reading, result.Error
	}
	return reading, nil
}
