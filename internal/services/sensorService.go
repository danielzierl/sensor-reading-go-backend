package services

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"time"

	// "time"

	// "io"
	"log"
	"net/http"
	"os"

	"gitlab.com/danielzierl/sensor-reading-go-backend/internal/resolver"
	"gitlab.com/danielzierl/sensor-reading-shared-libs/models"
)


func Login() (string, error) {

	requestBody := struct {
		Username string `json:"username"`
		Password string `json:"password"`
	}{
		Username: fmt.Sprintf("%s", os.Getenv("AIMTEC_AWS_USERNAME")),
		Password: fmt.Sprintf("%s", os.Getenv("AIMTEC_AWS_PASS")),
	}
	req, err := json.Marshal(&requestBody)
	if err != nil {
		log.Println("Error encoding JSON:", err)
		return "", err
	}
	bodyBuffer := bytes.NewBuffer(req)
	response, err := http.Post(fmt.Sprintf("%s/login", os.Getenv("AIMTEC_AWS_SERVER")), "application/json", bodyBuffer)

	if err != nil {
		log.Println("Error sending request to loging aws:", err)
		return "", err
	}
	defer response.Body.Close()

	if response.StatusCode != 200 {
		log.Println("Error sending request to loging aws:", response.StatusCode)
		return "", errors.New("Error sending request to loging aws")
	}

	data := convertResponseToStruct(response)
	uuid, ok := data["teamUUID"].(string)
	if !ok {
		return "", errors.New("uuid not found")
	}
	return uuid, nil
}

func ReadAllSensors() []string {
	var team_uuid string
	// login until responds nicely
	for {

		var err error
		team_uuid, err = Login()
		time.Sleep(5 * time.Second)
		if err == nil {
			break
		} else {
			log.Println("Error logging in:", err)
		}
	}
	request, err := http.NewRequest("GET", fmt.Sprintf("%s/sensors", os.Getenv("AIMTEC_AWS_SERVER")), nil)
	if err != nil {
		log.Println("Error creating request:", err)
		return nil
	}
	request.Header.Set("teamUUID", team_uuid)

	response, _ := sendRequest(request)
	defer response.Body.Close()

	body, err := io.ReadAll(response.Body)
	if err != nil {
		log.Println("Error reading response body:", err)
		return nil
	}

	var sensors []models.Sensor
	err = json.Unmarshal([]byte(body), &sensors)
	if err != nil {
		log.Panicln("Error parsing JSON:", err)
		return nil
	}

	resolver.RefreshSensors(sensors)

	return nil

}

func sendRequest(request *http.Request) (*http.Response, error) {
	client := &http.Client{}
	response, err := client.Do(request)
	if err != nil {
		return nil, err
	}
	return response, nil
}

func convertResponseToStruct(response *http.Response) map[string]interface{} {
	var data map[string]interface{}
	err := json.NewDecoder(response.Body).Decode(&data)
	if err != nil {
		log.Panicln("Error parsing JSON:", err)
		return nil
	}
	defer response.Body.Close()
	return data
}
