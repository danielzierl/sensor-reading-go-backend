package specialtypes
type FrontendSensorReading struct {
  Value float64 `json:"value"`
  Created_at string `json:"CreatedAt"`
}
