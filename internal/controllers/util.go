package controllers

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/danielzierl/sensor-reading-go-backend/internal/services"
)

func SetUtilRoutes(app *fiber.App) {
	app.Get("/cpu", (getCpuUsage))
	app.Get("/ram", (getRamUsage))
  app.Get("/readings-count", (getReadingsCount))
}
func getCpuUsage(c *fiber.Ctx) error {
  return c.SendString(services.GetCpuUsage())
}
func getRamUsage(c *fiber.Ctx) error {
  return c.SendString(services.GetRamUsage())
}
func getReadingsCount(c *fiber.Ctx) error {
  return c.SendString(services.GetReadingsCount())
}


