package resolver

import "gitlab.com/danielzierl/sensor-reading-shared-libs/models"

func GetReadingsCount() int64{
  var count int64
  db.Model(&models.SensorReading{}).Count(&count)
  return count
}
