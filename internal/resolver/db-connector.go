package resolver

import (
	"log"
	"os"

	"gitlab.com/danielzierl/sensor-reading-shared-libs/models"
	"gorm.io/driver/postgres"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

var db *gorm.DB
var db_log *gorm.DB

func GetDB() *gorm.DB {
  return db
}
func SetupAndConnectDb() {
	var DB_PATH = os.Getenv("DB_LOCATION")
	var DB_LOG_PATH = os.Getenv("DB_LOG_LOCATION")
	_db, err := gorm.Open(postgres.Open(DB_PATH), &gorm.Config{
    SkipDefaultTransaction: true,
    PrepareStmt: true,
  })
	if err != nil {
		log.Panic("Failed to connect database")
	}
  sqlDB, err := _db.DB()
  if err!=nil {
    log.Panic("Failed to create sqldb struct")
  }
  sqlDB.SetMaxIdleConns(2)
  sqlDB.SetMaxOpenConns(5)
	_db_log, err := gorm.Open(sqlite.Open(DB_LOG_PATH), &gorm.Config{})
	if err != nil {
		log.Panic("Failed to connect logging database")
	}
	// Migrate the schema
	err = _db.AutoMigrate(&models.SensorReading{})
	yell_error(err)
	err = _db.AutoMigrate(&models.User{})
	yell_error(err)
	err = _db_log.AutoMigrate(&models.Log{})
	yell_error(err)
	err = _db.AutoMigrate(&models.Sensor{})
	yell_error(err)
	err = _db.AutoMigrate(&models.Alert{})
	yell_error(err)

	db = _db
	db_log = _db_log
}

func yell_error(err error) {
	if err != nil {
		log.Panic("Migration error: ", err)
	}
}
