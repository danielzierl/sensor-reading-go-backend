package services

import (
	"errors"
	"os"
)

func GetTokenSecret()([]byte, error){
  jwt_secret := ([]byte(os.Getenv("SECRET")))
  if jwt_secret == nil {
    return nil, errors.New("jwt_secret is nil")
  }
  return jwt_secret,nil

}
