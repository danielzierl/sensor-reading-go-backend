package controllers

import (
	"encoding/json"
	"fmt"
	"log"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/websocket/v2"
	"gitlab.com/danielzierl/sensor-reading-go-backend/internal/services"
	"gitlab.com/danielzierl/sensor-reading-shared-libs/models"
)

func SetSensorReadingRoutes(app *fiber.App) {
	app.Get("/readings", websocket.New(getAllReadings))
}
func getAllReadings(c *websocket.Conn) {
  // get all query params

  // frontend needs separated data for each team
  team := c.Query("team")
  
  // frontend will separate to TEMPERATURE, HUMIDITY, ILLUMINATION
  type_reading := c.Query("type")

  // frontend will supply a value of desired timeframe eg. 1h (shows N results in last 1h)
  time_frame_hours := c.Query("time_frame_hours")

  // frontend could say preffered number of results
  n_results := c.Query("n_results")

  // check if token is valid
  token := c.Query("Auth")
  sct, _ := services.GetTokenSecret()
  err:=VerifyToken(sct,token)
  var last_reading_buffer models.SensorReading
  if err != nil {
    log.Printf("Could not verify token: %v", err)
    return
  }
  cacheKey := fmt.Sprintf("sensor_readings_%s_%s_%s_%s", team, type_reading, time_frame_hours, n_results)
	defer c.Close()
  var first_loop bool = true
  for {
    if _,_,err = c.ReadMessage(); err !=nil {
      break
    }
    var change bool = false
    for  !change && !first_loop {
      change, last_reading_buffer = services.ChangeInDb(team, last_reading_buffer)
      if change {
        break
      }
      time.Sleep(500 * time.Millisecond)
    }
    first_loop =false;
    result := services.GetAllSensorReadings(team, type_reading, time_frame_hours, n_results, cacheKey)
    
		if result == nil {
      // in this case there is a problem with logic
      log.Printf("Error getting all sensor readings in controller with params: %s, %s and %s", team, type_reading, time_frame_hours)
			continue
		}
		jsonData, err := json.Marshal(*result)
		if err != nil {
      // something got formated wrong
			log.Printf("Could not marshal json: %v", err)
			continue
		}

		err = c.WriteMessage(websocket.TextMessage, []byte(jsonData))
		if err != nil {
      // client disconnected, lets not log it, it makes mess in the logs, pipes get broken
			// log.Printf("Could not write message to websocket: %v", err)
      break
		}


	}
}
