package controllers

import (
	"encoding/json"
	"log"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/danielzierl/sensor-reading-go-backend/internal/services"
)

func SetLogRoutes(app *fiber.App) {
	app.Get("/logs", GetAllLogs)
}

func GetAllLogs(c *fiber.Ctx) error {
	result := services.GetAllLogs()
	if result == nil {
		log.Printf("Error getting all logs")
	}
	json, err := json.Marshal(result)
	if err != nil {
		log.Printf("Could not marshal json: %v", err)
	}
	return c.Send(json)
}
