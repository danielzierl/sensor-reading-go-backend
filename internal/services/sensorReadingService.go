package services

import (
	"fmt"
	"log"
	"time"

	"gitlab.com/danielzierl/sensor-reading-go-backend/internal/caching"
	"gitlab.com/danielzierl/sensor-reading-go-backend/internal/resolver"
	specialtypes "gitlab.com/danielzierl/sensor-reading-go-backend/internal/special_types"
	"gitlab.com/danielzierl/sensor-reading-shared-libs/models"

	"gorm.io/gorm"
)

func GetAllSensorReadings(team string, type_reading string, time_frame_hours string, n_results string, cacheKey string) *[]specialtypes.FrontendSensorReading {

	results := []specialtypes.FrontendSensorReading{}

	// get the gorm prefiltered db for type
	var err error
	var gormDBPrefiltered *gorm.DB
	switch type_reading {
	case "TEMPERATURE":
		gormDBPrefiltered, err = resolver.GetAllTemperatureReadings("sensor_readings")
		break
	case "HUMIDITY":
		gormDBPrefiltered, err = resolver.GetAllHumidityReadings("sensor_readings")
		break
	case "ILLUMINATION":
		log.Printf("Illumination is not yet supported")
		return nil
	default:
		log.Printf("Incorrect reading type supplied in websocket request params: %s", type_reading)
		return nil
	}
	if err != nil {
		log.Printf("Error getting all sensor readings: %v", err)
		return nil
	}

	// find only the correct team
	gormDBPrefiltered = resolver.TeamFinder(team, gormDBPrefiltered)

	// linearly skip the results so that they fit the timeframe
	gormDBPrefiltered = resolver.LinearTimeframe(time_frame_hours, n_results, gormDBPrefiltered)
	// limit the n_results, just to be sure
	gormDBPrefiltered = resolver.ResultLimiter(n_results, gormDBPrefiltered)

	// convert the results to struct
	caching.FindCachedReading(cacheKey, gormDBPrefiltered, &results)
	// gormDBPrefiltered.Find(&results)

	// finally reverse the order
	results = reverseArray(results)
	// results = *fix_teams_incompability(results, team)

	return &results
}
func fix_teams_incompability(results []specialtypes.FrontendSensorReading, team string) *[]specialtypes.FrontendSensorReading {
	// pink are so ....
	if team == "pink" {
		for i := range results {
			parsed, err := time.Parse("2006-01-02T15:04:05.999-07:00", results[i].Created_at)
			// _, offset := parsed.Zone()
			// parsed = parsed.Add(time.Hour * time.Duration(offset) / 3600)
			parsed = parsed.Add(time.Second * 3600)
			if err != nil {
				// log.Printf("Error parsing Created_at: %v", err)
				continue
			}
			results[i].Created_at = parsed.Format("2006-01-02T15:04:05")
		}
	}
	return &results

}
func ChangeInDb(team string, last_reading_previous models.SensorReading) (bool, models.SensorReading) {
	var last_reading models.SensorReading
	db := resolver.GetDB()
	caching.CacheTeamLastReading(fmt.Sprintf("%s;last", team), db, &last_reading, team)
	if last_reading.ID == last_reading_previous.ID {
		return false, last_reading
	}
	if (last_reading.CreatedAt.Sub(time.Now())) < time.Second*2 {
		return true, last_reading
	}
	return false, last_reading
}

// sadly, this function cannot be abstracted to any interface{} since it needs to much boilerplate
func reverseArray(arr []specialtypes.FrontendSensorReading) []specialtypes.FrontendSensorReading {
	for i, j := 0, len(arr)-1; i < j; i, j = i+1, j-1 {
		arr[i], arr[j] = arr[j], arr[i]

	}
	return arr
}
